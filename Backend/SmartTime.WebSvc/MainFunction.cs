﻿using System.Collections.Generic;
using System.Net;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;

namespace SmartTime.WebSvc;

public static class MainFunction
{
    [Function("MainFunction")]
    public static HttpResponseData Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", "post",Route = "info")] HttpRequestData req,
        FunctionContext executionContext)
    {
        var logger = executionContext.GetLogger("MainFunction");
        logger.LogInformation("C# HTTP trigger function processed a request.");
        var response = req.CreateResponse(HttpStatusCode.OK);
        response.Headers.AddStandardHeaders();
        response.WriteString("SmartTime web backend.");

        return response;
        
    }
}