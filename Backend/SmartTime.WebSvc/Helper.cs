﻿using Microsoft.Azure.Functions.Worker.Http;

namespace SmartTime.WebSvc;

public static class Helper
{
    public static HttpHeadersCollection AddStandardHeaders(this HttpHeadersCollection headersCollection)
    {
        headersCollection.Add("Content-Type", "text/plain; charset=utf-8");
        headersCollection.Add("X-SmartTime-ReqId", Guid.NewGuid().ToString());
        return headersCollection;
    }
}