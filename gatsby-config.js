module.exports = {
    siteMetadata: {
        title: "Smart Time",
        description: "Smart Time Platform",
        keywords: "permission-manager,software"
    },
    //pathPrefix: `/gtc`,
    plugins: [
        {
            resolve: "gatsby-plugin-manifest",
            options: {
                name: "Smart Time Platform - Welcome",
                icon: "src/images/favicon.svg",
                short_name: "Smart Time",
                start_url: "/",
                background_color: "#ffffff",
                theme_color: "#000000",
                display: "browser",
            },
        },
        {
            resolve: "gatsby-theme-carbon",
            options: {
                repository: {
                    baseUrl: "https://gitlab.com/sonnie.ton/smart-time-web",
                    subDirectory: "-/tree/main",
                },
                //not good enough
                // navigationStyle: 'header',
                titleType: "prepend"
            },
        },
        /*{
            resolve: `gatsby-transformer-remark`,
            options: {
                plugins: [`gatsby-remark-responsive-iframe`],
            },
        },*/
        {
            resolve: `gatsby-plugin-netlify`,
            options: {
                headers: {}, // option to add more headers. `Link` headers are transformed by the below criteria
                allPageHeaders: [], // option to add headers for all pages. `Link` headers are transformed by the below criteria
                mergeSecurityHeaders: false, // boolean to turn off the default security headers
                mergeCachingHeaders: true, // boolean to turn off the default caching headers
                transformHeaders: (headers, path) => headers, // optional transform for manipulating headers under each path (e.g.sorting), etc.
                generateMatchPathRewrites: true, // boolean to turn off automatic creation of redirect rules for client only paths
            },
        },
    ],
};
