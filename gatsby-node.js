exports.createPages = async ({ graphql, actions }) => {
    const { createRedirect } = actions;
    // why need a discord nitro for custom url?
    createRedirect({
        fromPath: `/discord`,
        isPermanent: false,
        toPath: `https://discord.gg/8wBgmnCrRp`,
    });
    createRedirect({
        fromPath: `/youtube`,
        isPermanent: false,
        toPath: `https://www.youtube.com/channel/UC6Hwk9A_dB9jQWD1fznLlCw`,
    });

    createRedirect({
       fromPath: `/getdotnet7`,
        isPermanent: true,
        toPath: `https://dotnet.microsoft.com/download/dotnet/7.0`,
    });
}