import React from 'react';
import { FourOhFour } from 'gatsby-theme-carbon';

const links = [
  { href: '/', text: 'Back home' },
  { href: '/intro', text: 'History' },
  { href: '/support', text: 'Support' },
];

const Custom404 = () => <FourOhFour links={links} />;

export default Custom404;
