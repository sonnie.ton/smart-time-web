import React from "react";
import {HomepageBanner, HomepageCallout} from "gatsby-theme-carbon";
import HomepageTemplate from "gatsby-theme-carbon/src/templates/Homepage";
import {calloutLink} from "./Homepage.module.scss";
import BannerImage from ".././images/hero/DSC00025a.png";

const FirstLeftText = () => <p>What is Smart Time?</p>;

const FirstRightText = () => (
    <p>
        A complete and simple permission management platform that <strong>built to suit the needs</strong>.
        {" "}made by student for student.

        Smart time makes it easy to grant and manage student permission for various location.
        Its much more then a <strong>permission manager</strong>. The limit is your ideas.

        {/*This is a callout component. You can edit the contents by updating the{" "}*/}
        {/*<a href="https://github.com/carbon-design-system/gatsby-theme-carbon/blob/5fe12de31bb19fbfa2cab7c69cd942f55aa06f79/packages/example/src/gatsby-theme-carbon/templates/Homepage.js">*/}
        {/*  pre-shadowed homepage template*/}
        {/*</a>*/}

        {/*. You can also provide <code>color</code> and <code>backgroundColor</code>{" "}*/}
        {/*props to suit your theme.*/}
        <a
            className={calloutLink}
            href={"/project#Watch a quick intro video"}
        >
            Watch a intro video →
        </a>
    </p>
);

const SecondLeftText = () => <p>Join our community; to stay updated</p>;


/*  <iframe src="https://discord.com/widget?id=1036936637353046046&theme=dark" width="350" height="500"
                allowTransparency="true" frameBorder="0"
                sandbox="allow-scripts"></iframe>*/

const SecondRightText = () => (
    <p>
        Discord Community
        <a
            className={calloutLink}
            href="/discord">
            Join Discord →
        </a>
    </p>

);

const BannerText = () => <h1>Homepage</h1>;

const customProps = {
    Banner: <HomepageBanner renderText={BannerText} image={BannerImage}/>,
    FirstCallout: (
        <HomepageCallout
            backgroundColor="#030303"
            color="white"
            leftText={FirstLeftText}
            rightText={FirstRightText}
        />
    ),
    SecondCallout: (
        <HomepageCallout
            leftText={SecondLeftText}
            rightText={SecondRightText}
            color="white"
            backgroundColor="#061f80"
        />
    ),
};

// spreading the original props gives us props.children (mdx content)
function ShadowedHomepage(props) {
    return <HomepageTemplate {...props} {...customProps} />;
}

export default ShadowedHomepage;
