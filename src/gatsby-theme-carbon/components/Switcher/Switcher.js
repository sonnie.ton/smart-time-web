import React from 'react';

import Switcher from 'gatsby-theme-carbon/src/components/Switcher/Switcher';
import {SwitcherLink} from 'gatsby-theme-carbon/src/components/Switcher/Switcher';
import {SwitcherDivider} from 'gatsby-theme-carbon/src/components/Switcher/Switcher';

const CustomSwitcher = (props) => (
    <Switcher {...props}>
        <SwitcherLink {...props} href="/youtube">
            Youtube
        </SwitcherLink>
        <SwitcherLink {...props} href="/discord">
            Discord
        </SwitcherLink>
        <SwitcherLink {...props} disabled href="/">
            Tiktok
        </SwitcherLink>
        <SwitcherDivider>Access</SwitcherDivider>
        <SwitcherLink {...props} disabled href="https://smart-time.netlify.app">
            Smart Time Web App
        </SwitcherLink>
        <SwitcherDivider>Related project</SwitcherDivider>
        <SwitcherLink {...props} isInternal
                      href="/">
            Medical mobile app
        </SwitcherLink>
        <SwitcherLink {...props}
                      href="/">
            Study optimizer(TM)
        </SwitcherLink>
    </Switcher>
);

/*const CustomSwitcher = () => <Switcher children={SwitcherChildrens} />;*/

export default CustomSwitcher;
