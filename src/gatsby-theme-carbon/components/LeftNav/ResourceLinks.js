import React from 'react';
import ResourceLinks from 'gatsby-theme-carbon/src/components/LeftNav/ResourceLinks';

const rsLinks = [
  {
    title: 'Public Discord',
    href: 'https://discord.gg/8wBgmnCrRp',
  },
  {
    title: 'Download center',
    href: '/download',
  }
];
// shouldOpenNewTabs: true if outbound links should open in a new tab
const CustomResources = () => <ResourceLinks shouldOpenNewTabs links={rsLinks} />;
export default CustomResources;
