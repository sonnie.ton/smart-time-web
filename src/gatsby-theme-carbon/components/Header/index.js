import React from 'react';
import Header from 'gatsby-theme-carbon/src/components/Header';
import { Chip20 } from '@carbon/icons-react';
import HeaderLogo from '../../../images/favicon.svg';

const CustomHeader = (props) => (
  <Header {...props}>
      <img alt={"St logo"}
    height={'100%'}
    src={HeaderLogo}/>
      <span>Smart Time</span>&nbsp;Platform
      {/*<Chip20 style={{marginLeft: 5}}/>*/}
  </Header>
);

export default CustomHeader;
